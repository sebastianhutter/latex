FROM alpine:3

# install latex
RUN apk add --no-cache fontconfig texlive-full make

# install fonts
COPY fonts/* /usr/share/fonts/
RUN fc-cache
